﻿#Include Lib\_COM_Meta.ahk

class ICoreWebView2WebMessageReceivedEventArgs extends _COM_Meta
{
	Source[]
	{
		get
		{
			if (r := DllCall(NumGet(NumGet(this.ptr)+3*A_PtrSize), "Ptr", this.ptr
				, "Ptr*", pSource ; LPWSTR *source
				, "Int"))
				throw Exception(r)
			this._Get_Str(pSource, source)
			return source
		}
	}
	
	WebMessageAsJson[]
	{
		get
		{
			if (r := DllCall(NumGet(NumGet(this.ptr)+4*A_PtrSize), "Ptr", this.ptr
				, "Ptr*", pJson ; LPWSTR *source
				, "Int"))
				throw Exception(r)
			this._Get_Str(pJson, json)
			return json
		}
	}
	
	TryGetWebMessageAsString(ByRef webMessageAsString)
	{
		if (r := DllCall(NumGet(NumGet(this.ptr)+5*A_PtrSize), "Ptr", this.ptr
			, "Ptr*", pString ; LPWSTR *webMessageAsString
			, "Int"))
			throw Exception(r)
		this._Get_Str(pString, webMessageAsString)
	}
}
