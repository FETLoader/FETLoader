﻿#Include Lib\_COM_Meta.ahk

class ICoreWebView2NavigationStartingEventArgs extends _COM_Meta
{
	Uri[]
	{
		get
		{
			if (r := DllCall(NumGet(NumGet(this.ptr)+3*A_PtrSize), "Ptr", this.ptr
				, "Ptr*", pURI ; LPCWSTR *uri
				, "Int"))
				throw Exception(r)
			this._Get_Str(pURI, value)
			return value
		}
	}
	
	IsUserInitiated[]
	{
		get
		{
			if (r := DllCall(NumGet(NumGet(this.ptr)+4*A_PtrSize), "Ptr", this.ptr
				, "Int*", isUserInitiated ; BOOL *isUserInitiated
				, "Int"))
				throw Exception(r)
			return isUserInitiated
		}
	}
	
	IsRedirected[]
	{
		get
		{
			if (r := DllCall(NumGet(NumGet(this.ptr)+5*A_PtrSize), "Ptr", this.ptr
				, "Int*", isRedirected ; BOOL *isUserInitiated
				, "Int"))
				throw Exception(r)
			return isRedirected
		}
	}
	
	RequestHeaders[]
	{
		get
		{
			if (r := DllCall(NumGet(NumGet(this.ptr)+6*A_PtrSize), "Ptr", this.ptr
				, "Ptr*", requestHeaders ; ICoreWebView2HttpRequestHeaders **requestHeaders
				, "Int"))
				throw Exception(r)
			return requestHeaders
		}
	}
	
	Cancel[]
	{
		get
		{
			if (r := DllCall(NumGet(NumGet(this.ptr)+7*A_PtrSize), "Ptr", this.ptr
				, "Int*", cancel ; BOOL *cancel
				, "Int"))
				throw Exception(r)
			return cancel
		}
		
		set
		{
			if (r := DllCall(NumGet(NumGet(this.ptr)+8*A_PtrSize), "Ptr", this.ptr
				, "Int", value ; BOOL cancel
				, "Int"))
				throw Exception(r)
			return value
		}
	}
	
	NavigationId[]
	{
		get
		{
			if (r := DllCall(NumGet(NumGet(this.ptr)+9*A_PtrSize), "Ptr", this.ptr
				, "Int64*", navigation_id ; UINT64 *navigation_id
				, "Int"))
				throw Exception(r)
			return navigation_id
		}
	}
}
