﻿#Include Lib\_COM_Meta.ahk
#Include Lib\AsyncRoutine.ahk
#Include Lib\IUnknown.ahk

class ICoreWebView2 extends _COM_Meta
{
	; 3
	; /* [propget] */ HRESULT ( STDMETHODCALLTYPE *get_Settings )(
	;     ICoreWebView2 * This,
	;     /* [retval][out] */ ICoreWebView2Settings **settings);
	; 4
	; /* [propget] */ HRESULT ( STDMETHODCALLTYPE *get_Source )(
	;     ICoreWebView2 * This,
	;     /* [retval][out] */ LPWSTR *uri);
	
	Navigate(ByRef uri)
	{
		if (r := DllCall(NumGet(NumGet(this.ptr)+5*A_PtrSize), "Ptr", this.ptr
			, "Str", uri ; LPCWSTR uri
			, "Int"))
			throw Exception(r)
	}
	
	NavigateToString(ByRef htmlContent)
	{
		if (r := DllCall(NumGet(NumGet(this.ptr)+6*A_PtrSize), "Ptr", this.ptr
			, "Str", htmlContent ; LPCWSTR htmlContent
			, "Int"))
			throw Exception(r)
	}
	
	add_NavigationStarting(eventHandler, ByRef token)
	{
		if (r := DllCall(NumGet(NumGet(this.ptr)+7*A_PtrSize), "Ptr", this.ptr
			, "Ptr", eventHandler ; ICoreWebView2NavigationStartingEventHandler *eventHandler
			, "Int64*", token     ; EventRegistrationToken *token
			, "Int"))
			throw Exception(r)
	}
	
	; 8
	; HRESULT ( STDMETHODCALLTYPE *remove_NavigationStarting )(
	;     ICoreWebView2 * This,
	;     /* [in] */ EventRegistrationToken token);
	; 9
	; HRESULT ( STDMETHODCALLTYPE *add_ContentLoading )(
	;     ICoreWebView2 * This,
	;     /* [in] */ ICoreWebView2ContentLoadingEventHandler *eventHandler,
	;     /* [out] */ EventRegistrationToken *token);
	; 10
	; HRESULT ( STDMETHODCALLTYPE *remove_ContentLoading )(
	;     ICoreWebView2 * This,
	;     /* [in] */ EventRegistrationToken token);
	; 11
	; HRESULT ( STDMETHODCALLTYPE *add_SourceChanged )(
	;     ICoreWebView2 * This,
	;     /* [in] */ ICoreWebView2SourceChangedEventHandler *eventHandler,
	;     /* [out] */ EventRegistrationToken *token);
	; 12
	; HRESULT ( STDMETHODCALLTYPE *remove_SourceChanged )(
	;     ICoreWebView2 * This,
	;     /* [in] */ EventRegistrationToken token);
	; 13
	; HRESULT ( STDMETHODCALLTYPE *add_HistoryChanged )(
	;     ICoreWebView2 * This,
	;     /* [in] */ ICoreWebView2HistoryChangedEventHandler *eventHandler,
	;     /* [out] */ EventRegistrationToken *token);
	; 14
	; HRESULT ( STDMETHODCALLTYPE *remove_HistoryChanged )(
	;     ICoreWebView2 * This,
	;     /* [in] */ EventRegistrationToken token);
	; 15
	; HRESULT ( STDMETHODCALLTYPE *add_NavigationCompleted )(
	;     ICoreWebView2 * This,
	;     /* [in] */ ICoreWebView2NavigationCompletedEventHandler *eventHandler,
	;     /* [out] */ EventRegistrationToken *token);
	; 16
	; HRESULT ( STDMETHODCALLTYPE *remove_NavigationCompleted )(
	;     ICoreWebView2 * This,
	;     /* [in] */ EventRegistrationToken token);
	; 17
	; HRESULT ( STDMETHODCALLTYPE *add_FrameNavigationStarting )(
	;     ICoreWebView2 * This,
	;     /* [in] */ ICoreWebView2NavigationStartingEventHandler *eventHandler,
	;     /* [out] */ EventRegistrationToken *token);
	; 18
	; HRESULT ( STDMETHODCALLTYPE *remove_FrameNavigationStarting )(
	;     ICoreWebView2 * This,
	;     /* [in] */ EventRegistrationToken token);
	; 19
	; HRESULT ( STDMETHODCALLTYPE *add_FrameNavigationCompleted )(
	;     ICoreWebView2 * This,
	;     /* [in] */ ICoreWebView2NavigationCompletedEventHandler *eventHandler,
	;     /* [out] */ EventRegistrationToken *token);
	; 20
	; HRESULT ( STDMETHODCALLTYPE *remove_FrameNavigationCompleted )(
	;     ICoreWebView2 * This,
	;     /* [in] */ EventRegistrationToken token);
	; 21
	; HRESULT ( STDMETHODCALLTYPE *add_ScriptDialogOpening )(
	;     ICoreWebView2 * This,
	;     /* [in] */ ICoreWebView2ScriptDialogOpeningEventHandler *eventHandler,
	;     /* [out] */ EventRegistrationToken *token);
	; 22
	; HRESULT ( STDMETHODCALLTYPE *remove_ScriptDialogOpening )(
	;     ICoreWebView2 * This,
	;     /* [in] */ EventRegistrationToken token);
	; 23
	; HRESULT ( STDMETHODCALLTYPE *add_PermissionRequested )(
	;     ICoreWebView2 * This,
	;     /* [in] */ ICoreWebView2PermissionRequestedEventHandler *eventHandler,
	;     /* [out] */ EventRegistrationToken *token);
	; 24
	; HRESULT ( STDMETHODCALLTYPE *remove_PermissionRequested )(
	;     ICoreWebView2 * This,
	;     /* [in] */ EventRegistrationToken token);
	; 25
	; HRESULT ( STDMETHODCALLTYPE *add_ProcessFailed )(
	;     ICoreWebView2 * This,
	;     /* [in] */ ICoreWebView2ProcessFailedEventHandler *eventHandler,
	;     /* [out] */ EventRegistrationToken *token);
	; 26
	; HRESULT ( STDMETHODCALLTYPE *remove_ProcessFailed )(
	;     ICoreWebView2 * This,
	;     /* [in] */ EventRegistrationToken token);
	
	AddScriptToExecuteOnDocumentCreated(javaScript, token)
	{
		if (r := DllCall(NumGet(NumGet(this.ptr)+27*A_PtrSize), "Ptr", this.ptr
			, "WStr", javaScript ; LPCWSTR javaScript
			, "Ptr", token       ; EventRegistrationToken *token
			, "Int"))
			throw Exception(r)
	}
	
	; 28
	; HRESULT ( STDMETHODCALLTYPE *RemoveScriptToExecuteOnDocumentCreated )(
	;     ICoreWebView2 * This,
	;     /* [in] */ LPCWSTR id);
	
	ExecuteScript(javaScript, handler)
	{
		if (r := DllCall(NumGet(NumGet(this.ptr)+29*A_PtrSize), "Ptr", this.ptr
			, "WStr", javaScript ; LPCWSTR javaScript
			, "Ptr", handler     ; ICoreWebView2ExecuteScriptCompletedHandler * handler
			, "Int"))
			throw Exception(r)
	}
	
	; 30
	; HRESULT ( STDMETHODCALLTYPE *CapturePreview )(
	;     ICoreWebView2 * This,
	;     /* [in] */ COREWEBVIEW2_CAPTURE_PREVIEW_IMAGE_FORMAT imageFormat,
	;     /* [in] */ IStream *imageStream,
	;     /* [in] */ ICoreWebView2CapturePreviewCompletedHandler *handler);
	; 31
	; HRESULT ( STDMETHODCALLTYPE *Reload )(
	;     ICoreWebView2 * This);
	; 32
	; HRESULT ( STDMETHODCALLTYPE *PostWebMessageAsJson )(
	;     ICoreWebView2 * This,
	;     /* [in] */ LPCWSTR webMessageAsJson);
	; 33
	; HRESULT ( STDMETHODCALLTYPE *PostWebMessageAsString )(
	;     ICoreWebView2 * This,
	;     /* [in] */ LPCWSTR webMessageAsString);
	
	add_WebMessageReceived(handler, ByRef token)
	{
		if (r := DllCall(NumGet(NumGet(this.ptr)+34*A_PtrSize), "Ptr", this.ptr
			, "Ptr", handler  ; ICoreWebView2WebMessageReceivedEventHandler *handler
			, "Int64*", token ; EventRegistrationToken *token
			, "Int"))
			throw Exception(r)
	}
	
	; 35
	; HRESULT ( STDMETHODCALLTYPE *remove_WebMessageReceived )(
	;     ICoreWebView2 * This,
	;     /* [in] */ EventRegistrationToken token);
	
	remove_WebMessageReceived(token)
	{
		if (r := DllCall(NumGet(NumGet(this.ptr)+35*A_PtrSize), "Ptr", this.ptr
			, "Int64", token ; EventRegistrationToken token
			, "Int"))
			throw Exception(r)
	}
	
	; 36
	; HRESULT ( STDMETHODCALLTYPE *CallDevToolsProtocolMethod )(
	;     ICoreWebView2 * This,
	;     /* [in] */ LPCWSTR methodName,
	;     /* [in] */ LPCWSTR parametersAsJson,
	;     /* [in] */ ICoreWebView2CallDevToolsProtocolMethodCompletedHandler *handler);
	; 37
	; /* [propget] */ HRESULT ( STDMETHODCALLTYPE *get_BrowserProcessId )(
	;     ICoreWebView2 * This,
	;     /* [retval][out] */ UINT32 *value);
	; 38
	; /* [propget] */ HRESULT ( STDMETHODCALLTYPE *get_CanGoBack )(
	;     ICoreWebView2 * This,
	;     /* [retval][out] */ BOOL *canGoBack);
	; 39
	; /* [propget] */ HRESULT ( STDMETHODCALLTYPE *get_CanGoForward )(
	;     ICoreWebView2 * This,
	;     /* [retval][out] */ BOOL *canGoForward);
	; 40
	; HRESULT ( STDMETHODCALLTYPE *GoBack )(
	;     ICoreWebView2 * This);
	; 41
	; HRESULT ( STDMETHODCALLTYPE *GoForward )(
	;     ICoreWebView2 * This);
	; 42
	; HRESULT ( STDMETHODCALLTYPE *GetDevToolsProtocolEventReceiver )(
	;     ICoreWebView2 * This,
	;     /* [in] */ LPCWSTR eventName,
	;     /* [retval][out] */ ICoreWebView2DevToolsProtocolEventReceiver **receiver);
	; 43
	; HRESULT ( STDMETHODCALLTYPE *Stop )(
	;     ICoreWebView2 * This);
	; 44
	; HRESULT ( STDMETHODCALLTYPE *add_NewWindowRequested )(
	;     ICoreWebView2 * This,
	;     /* [in] */ ICoreWebView2NewWindowRequestedEventHandler *eventHandler,
	;     /* [out] */ EventRegistrationToken *token);
	; 45
	; HRESULT ( STDMETHODCALLTYPE *remove_NewWindowRequested )(
	;     ICoreWebView2 * This,
	;     /* [in] */ EventRegistrationToken token);
	; 46
	; HRESULT ( STDMETHODCALLTYPE *add_DocumentTitleChanged )(
	;     ICoreWebView2 * This,
	;     /* [in] */ ICoreWebView2DocumentTitleChangedEventHandler *eventHandler,
	;     /* [out] */ EventRegistrationToken *token);
	; 47
	; HRESULT ( STDMETHODCALLTYPE *remove_DocumentTitleChanged )(
	;     ICoreWebView2 * This,
	;     /* [in] */ EventRegistrationToken token);
	; 48
	; /* [propget] */ HRESULT ( STDMETHODCALLTYPE *get_DocumentTitle )(
	;     ICoreWebView2 * This,
	;     /* [retval][out] */ LPWSTR *title);
	
	AddHostObjectToScript(name, object)
	{
		if (r := DllCall(NumGet(NumGet(this.ptr)+49*A_PtrSize), "Ptr", this.ptr
			, "WStr", name   ; LPCWSTR name
			, "Ptr", object ; VARIANT *object
			, "Int"))
			throw Exception(r)
	}
	
	RemoveHostObjectFromScript(name)
	{
		if (r := DllCall(NumGet(NumGet(this.ptr)+50*A_PtrSize), "Ptr", this.ptr
			, "WStr", name ; LPCWSTR name
			, "Int"))
			throw Exception(r)
	}
	
	; 51
	; HRESULT ( STDMETHODCALLTYPE *OpenDevToolsWindow )(
	;     ICoreWebView2 * This);
	; 52
	; HRESULT ( STDMETHODCALLTYPE *add_ContainsFullScreenElementChanged )(
	;     ICoreWebView2 * This,
	;     /* [in] */ ICoreWebView2ContainsFullScreenElementChangedEventHandler *eventHandler,
	;     /* [out] */ EventRegistrationToken *token);
	; 53
	; HRESULT ( STDMETHODCALLTYPE *remove_ContainsFullScreenElementChanged )(
	;     ICoreWebView2 * This,
	;     /* [in] */ EventRegistrationToken token);
	; 54
	; /* [propget] */ HRESULT ( STDMETHODCALLTYPE *get_ContainsFullScreenElement )(
	;     ICoreWebView2 * This,
	;     /* [retval][out] */ BOOL *containsFullScreenElement);
	; 55
	; HRESULT ( STDMETHODCALLTYPE *add_WebResourceRequested )(
	;     ICoreWebView2 * This,
	;     /* [in] */ ICoreWebView2WebResourceRequestedEventHandler *eventHandler,
	;     /* [out] */ EventRegistrationToken *token);
	; 56
	; HRESULT ( STDMETHODCALLTYPE *remove_WebResourceRequested )(
	;     ICoreWebView2 * This,
	;     /* [in] */ EventRegistrationToken token);
	; 57
	; HRESULT ( STDMETHODCALLTYPE *AddWebResourceRequestedFilter )(
	;     ICoreWebView2 * This,
	;     /* [in] */ const LPCWSTR uri,
	;     /* [in] */ const COREWEBVIEW2_WEB_RESOURCE_CONTEXT resourceContext);
	; 58
	; HRESULT ( STDMETHODCALLTYPE *RemoveWebResourceRequestedFilter )(
	;     ICoreWebView2 * This,
	;     /* [in] */ const LPCWSTR uri,
	;     /* [in] */ const COREWEBVIEW2_WEB_RESOURCE_CONTEXT resourceContext);
	; 59
	; HRESULT ( STDMETHODCALLTYPE *add_WindowCloseRequested )(
	;     ICoreWebView2 * This,
	;     /* [in] */ ICoreWebView2WindowCloseRequestedEventHandler *eventHandler,
	;     /* [out] */ EventRegistrationToken *token);
	; 60
	; HRESULT ( STDMETHODCALLTYPE *remove_WindowCloseRequested )(
	;     ICoreWebView2 * This,
	;     /* [in] */ EventRegistrationToken token);
}
