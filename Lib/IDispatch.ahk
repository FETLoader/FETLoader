﻿#Include Lib\_COM_Meta.ahk

class IDispatch extends _COM_Meta
{
	static LOCALE_USER_DEFAULT := 0x400
	static DISPATCH_METHOD         := 0x1
	static DISPATCH_PROPERTYGET    := 0x2
	static DISPATCH_PROPERTYPUT    := 0x4
	static DISPATCH_PROPERTYPUTREF := 0x8
	static VT_DISPATCH := 0x09 ; Object
	static VT_I8       := 0x16 ; Int64
	static VT_BSTR     := 0x08 ; WStr
	static VT_R8       := 0x05 ; Double
	
	GetIDOfName(szName)
	{
		;Print("Getting ID of name " szName)
		VarSetCapacity(GUID_NULL, 16, 0)
		if (r := DllCall(NumGet(NumGet(this.ptr)+5*A_PtrSize)
			, "Ptr", this.ptr
			, "Ptr", &GUID_NULL
			, "Str*", szName
			, "UInt", 1
			, "Int", IDispatch.LOCALE_USER_DEFAULT
			, "Int*", rgDispId
			, "UInt"))
			throw Exception(r)
		;Print("Got ID " rgDispId)
		return rgDispId
	}
	
	Invoke(dispIdMember, wFlags, pDispParams:=0, pVarResult:=0, pExcepInfo:=0, puArgErr:=0)
	{
		VarSetCapacity(GUID_NULL, 16, 0)
		if (r := DllCall(NumGet(NumGet(this.ptr)+6*A_PtrSize)
			, "Ptr", this.ptr
			, "Int", dispIdMember
			, "Ptr", &GUID_NULL
			, "Int", IDispatch.LOCALE_USER_DEFAULT
			, "Short", wFlags
			, "Ptr", pDispParams
			, "Ptr", pVarResult
			, "Ptr", pExcepInfo
			, "Ptr", puArgErr
			, "UInt"))
			throw Exception(r)
	}
	
	__Get(name)
	{
		;Print("Getting " name)
		;Print("this.ptr " this.ptr)
		; Find and invoke the property
		id := this.GetIDOfName(name)
		;Print("Found ID " ID)
		VarSetCapacity(result, 16, 0)
		this.Invoke(id, IDispatch.DISPATCH_PROPERTYGET, 0, &result)
		
		if (NumGet(result, "UShort") == IDispatch.VT_DISPATCH)
			return new IDispatch(NumGet(result, 8))
		
		; Create a SafeArray and populate it with the result
		arr := ComObjArray(NumGet(result, "UShort"), 1)
		DllCall("oleaut32\SafeArrayAccessData", "Ptr", ComObjValue(arr), "Ptr*", arr_data)
		NumPut(NumGet(result, 8), arr_data+0)
		DllCall("oleaut32\SafeArrayUnaccessData", "Ptr", ComObjValue(arr))
		
		; Retrieve the AHK-wrapped value
		return arr[0]
	}
	
	__Call(name, params*)
	{
		;Print("Calling " name, params)
		if (IDispatch[name])
			return IDispatch[name].(this, params*)
		;Print("Invoking call to " name " as IDispatch object")
		
		; Find and get the function object
		id := this.GetIDOfName(name)
		VarSetCapacity(result, 16, 0)
		this.Invoke(id, IDispatch.DISPATCH_PROPERTYGET, 0, &result)
		
		; Verify that the retrieved object is an IDispatch object (could be function or map)
		if (NumGet(result, "UShort") != IDispatch.VT_DISPATCH)
			throw Exception("Property not function")
		
		; Construct the array of variant arguments
		cParams := params.Count()
		VarSetCapacity(rgvarg, 24*cParams, 0)
		for i, p in params
		{
			if IsObject(p)
			{
				NumPut(IDispatch.VT_DISPATCH, rgvarg, (cParams-i)*24+0, "UShort")
				NumPut(&p, rgvarg, (cParams-i)*24+8, "UPtr")
			}
			else if p is Integer
			{
				NumPut(IDispatch.VT_I8, rgvarg, (cParams-i)*24+0, "UShort")
				NumPut(p, rgvarg, (cParams-i)*24+8, "Int64")
			}
			else if p is Float
			{
				NumPut(IDispatch.VT_R8, rgvarg, (cParams-i)*24+0, "UShort")
				NumPut(p, rgvarg, (cParams-i)*24+8, "Double")
			}
			else
			{
				params.SetCapacity(i, StrPut(p, "UTF-16") * 2 )
				StrPut(p, ptr := params.GetAddress(i), "UTF-16")
				NumPut(IDispatch.VT_BSTR, rgvarg, (cParams-i)*24+0, "UShort")
				NumPut(ptr, rgvarg, (cParams-i)*24+8, "UPtr")
			}
		}
		
		; Build the DISPPARAMS structure
		VarSetCapacity(dispParams, A_PtrSize*2+4*2, 0)
		NumPut(&rgvarg, dispParams, 0)
		NumPut(0      , dispParams, 8)
		NumPut(cParams, dispParams, 16, "UInt")
		NumPut(0      , dispParams, 20, "UInt")
		
		; Try invoking the IDispatch object as a function
		method := new IDispatch(NumGet(result, 8))
		VarSetCapacity(result, 16, 0)
		method.Invoke(-1, IDispatch.DISPATCH_METHOD, &dispParams, &result)
		
		;Print("Invocation returned variant: type " NumGet(result, "UShort") " value " NumGet(result, "UPtr"))
		;Print("this.ptr " this.ptr)
		
		; Return an IDispatch-wrapper wrapped result
		;return new IDispatch(NumGet(result, 8))
	}
}