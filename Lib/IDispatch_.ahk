﻿#Include Lib\IUnknown.ahk

oid := new IDispatch()

ComObject := ComObject(9, oid.ptr, 1), ObjAddRef(oid.ptr)

MsgBox, % %ComObject%("a", 1, 1.2, {"key": "val"})

class IDispatch extends IUnknown
{
	methods := ["GetTypeInfoCount", "GetTypeInfo", "GetIDsOfNames", "Invoke"]
	
	GetTypeInfoCount(pctinfo)
	{
		MsgBox
	}
	
	GetTypeInfo(iTInfo, lcid, ppTInfo)
	{
		msgbox
		;Print(["lcid", lcid])
	}
	
	GetIDsOfNames(riid, rgszNames, cNames, lcid, rgDispId)
	{
		;Print(["GetIDsOfNames", riid, rgszNames, cNames, lcid, rgDispId])
		;Print(NumGet(riid+0))
		;Print(StrGet(NumGet(rgszNames+0)))
		NumPut(0, rgDispId+0, "Int")
		return 0
	}
	
	Invoke(dispIdMember, riid, lcid, wFlags, pDispParams, pVarResult, pExcepInfo, puArgErr)
	{
		;Print(["Invoke", dispIdMember, riid, lcid, wFlags, pDispParams, pVarResult, pExcepInfo, puArgErr])
		NumPut(3, pVarResult+0, "UShort")
		NumPut(123, pVarResult+8, "Int")
		
		if (pDispParams)
		{
			printf("pDispParams (" A_PtrSize*2+8 ")`n")
			printMem(pDispParams, A_PtrSize*2+8)
			count := NumGet(pDispParams+A_PtrSize*2, "UInt")
			;Print("Discovered " count " parameters:")
			loop, %count%
			{
				type := NumGet(NumGet(pDispParams+0)+(A_Index-1)*24, "UShort")
				if (type == 8)
					value := StrGet(NumGet(NumGet(pDispParams+0)+(A_Index-1)*24+8), "UTF-16")
				else if (type == 3)
					value := NumGet(NumGet(pDispParams+0)+(A_Index-1)*24+8, "Int")
				else if (type == 9)
					value := "Object"
				else
					value := ""
				Printf("Type {:04x} Value {}`n", type, value)
				printMem(NumGet(pDispParams+0)+(A_Index-1)*24, 24)
				
			}
		}
		return 0
	}
}
