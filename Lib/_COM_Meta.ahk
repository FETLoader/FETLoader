﻿
class _COM_Meta
{
	__New(ptr)
	{
		this.ptr := ptr
		ObjAddRef(ptr)
	}
	
	__Delete()
	{
		ObjRelease(this.ptr)
	}
	
	_Get_Str(pStr, ByRef str)
	{
		str := StrGet(pStr, "UTF-16")
		DllCall("ole32\CoTaskMemFree", "Ptr", pStr)
	}
}
