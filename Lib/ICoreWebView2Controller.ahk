﻿#Include Lib\_COM_Meta.ahk

class ICoreWebView2Controller extends _COM_Meta
{
	get_IsVisible(ByRef isVisible)
	{
		if (r := DllCall(NumGet(NumGet(this.ptr)+3*A_PtrSize), "Ptr", this.ptr
			, "Int*", isVisible ; BOOL *isVisible
			, "Int"))           ; HRESULT
			throw Exception(r)
	}
	
	put_IsVisible(isVisible)
	{
		if (r := DllCall(NumGet(NumGet(this.ptr)+4*A_PtrSize), "Ptr", this.ptr
			, "Int", isVisible ; BOOL isVisible
			, "Int"))          ; HRESULT
			throw Exception(r)
	}
	
	get_Bounds(ByRef bounds)
	{
		; 5
		; /* [propget] */ HRESULT ( STDMETHODCALLTYPE *get_Bounds )(
		;     ICoreWebView2Controller * This,
		;     /* [retval][out] */ RECT *bounds);
	}
	
	put_Bounds(ByRef bounds)
	{
		if (r := DllCall(NumGet(NumGet(this.ptr)+6*A_PtrSize), "Ptr", this.ptr
			, "Int", bounds ; RECT bounds
			, "Int"))       ; HRESULT
			throw Exception(r)
	}
	
	get_ZoomFactor(ByRef zoomFactor)
	{
		; 7
		; /* [propget] */ HRESULT ( STDMETHODCALLTYPE *get_ZoomFactor )(
		;     ICoreWebView2Controller * This,
		;     /* [retval][out] */ double *zoomFactor);
	}
	
	put_ZoomFactor(zoomFactor)
	{
		; 8
		; /* [propput] */ HRESULT ( STDMETHODCALLTYPE *put_ZoomFactor )(
		;     ICoreWebView2Controller * This,
		;     /* [in] */ double zoomFactor);
	}
	
	add_ZoomFactorChanged(eventHandler, ByRef token)
	{
		; 9
		; HRESULT ( STDMETHODCALLTYPE *add_ZoomFactorChanged )(
		;     ICoreWebView2Controller * This,
		;     /* [in] */ ICoreWebView2ZoomFactorChangedEventHandler *eventHandler,
		;     /* [out] */ EventRegistrationToken *token);
	}
	
	remove_ZoomFactorChanged(token)
	{
		; 10
		; HRESULT ( STDMETHODCALLTYPE *remove_ZoomFactorChanged )(
		;     ICoreWebView2Controller * This,
		;     /* [in] */ EventRegistrationToken token);
	}
	
	SetBoundsAndZoomFactor(bounds, zoomFactor)
	{
		; 11
		; HRESULT ( STDMETHODCALLTYPE *SetBoundsAndZoomFactor )(
		;     ICoreWebView2Controller * This,
		;     /* [in] */ RECT bounds,
		;     /* [in] */ double zoomFactor);
	}
	
	MoveFocus(reason)
	{
		; 12
		; HRESULT ( STDMETHODCALLTYPE *MoveFocus )(
		;     ICoreWebView2Controller * This,
		;     /* [in] */ COREWEBVIEW2_MOVE_FOCUS_REASON reason);
	}
	
	add_MoveFocusRequested(eventHandler, ByRef token)
	{
		; 13
		; HRESULT ( STDMETHODCALLTYPE *add_MoveFocusRequested )(
		;     ICoreWebView2Controller * This,
		;     /* [in] */ ICoreWebView2MoveFocusRequestedEventHandler *eventHandler,
		;     /* [out] */ EventRegistrationToken *token);
	}
	
	remove_MoveFocusRequested(token)
	{
		; 14
		; HRESULT ( STDMETHODCALLTYPE *remove_MoveFocusRequested )(
		;     ICoreWebView2Controller * This,
		;     /* [in] */ EventRegistrationToken token);
	}
	
	add_GotFocus(eventHandler, ByRef token)
	{
		; 15
		; HRESULT ( STDMETHODCALLTYPE *add_GotFocus )(
		;     ICoreWebView2Controller * This,
		;     /* [in] */ ICoreWebView2FocusChangedEventHandler *eventHandler,
		;     /* [out] */ EventRegistrationToken *token);
	}
	
	remove_GotFocus(token)
	{
		; 16
		; HRESULT ( STDMETHODCALLTYPE *remove_GotFocus )(
		;     ICoreWebView2Controller * This,
		;     /* [in] */ EventRegistrationToken token);
	}
	
	add_LostFocus(eventHandler, ByRef token)
	{
		; 17
		; HRESULT ( STDMETHODCALLTYPE *add_LostFocus )(
		;     ICoreWebView2Controller * This,
		;     /* [in] */ ICoreWebView2FocusChangedEventHandler *eventHandler,
		;     /* [out] */ EventRegistrationToken *token);
	}
	
	remove_LostFocus(token)
	{
		; 18
		; HRESULT ( STDMETHODCALLTYPE *remove_LostFocus )(
		;     ICoreWebView2Controller * This,
		;     /* [in] */ EventRegistrationToken token);
	}
	
	add_AcceleratorKeyPressed(eventHandler, ByRef token)
	{
		; 19
		; HRESULT ( STDMETHODCALLTYPE *add_AcceleratorKeyPressed )(
		;     ICoreWebView2Controller * This,
		;     /* [in] */ ICoreWebView2AcceleratorKeyPressedEventHandler *eventHandler,
		;     /* [out] */ EventRegistrationToken *token);
	}
	
	remove_AcceleratorKeyPressed(token)
	{
		; 20
		; HRESULT ( STDMETHODCALLTYPE *remove_AcceleratorKeyPressed )(
		;     ICoreWebView2Controller * This,
		;     /* [in] */ EventRegistrationToken token);
	}
	
	get_ParentWindow(ByRef topLevelWindow)
	{
		; 21
		; /* [propget] */ HRESULT ( STDMETHODCALLTYPE *get_ParentWindow )(
		;     ICoreWebView2Controller * This,
		;     /* [retval][out] */ HWND *topLevelWindow);
	}
	
	put_ParentWindow(topLevelWindow)
	{
		; 22
		; /* [propput] */ HRESULT ( STDMETHODCALLTYPE *put_ParentWindow )(
		;     ICoreWebView2Controller * This,
		;     /* [in] */ HWND topLevelWindow);
	}
	
	NotifyParentWindowPositionChanged()
	{
		; 23
		; HRESULT ( STDMETHODCALLTYPE *NotifyParentWindowPositionChanged )(
		;     ICoreWebView2Controller * This);
	}
	
	Close()
	{
		; 24
		; HRESULT ( STDMETHODCALLTYPE *Close )(
		;     ICoreWebView2Controller * This);
	}
	
	get_CoreWebView2(ByRef coreWebView2)
	{
		if (r := DllCall(NumGet(NumGet(this.ptr)+25*A_PtrSize), "Ptr", this.ptr
			, "Ptr*", coreWebView2 ; ICoreWebView2 **coreWebView2
			, "Int"))              ; HRESULT
			throw Exception(r)
	}
}
