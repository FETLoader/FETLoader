﻿
class IUnknown
{
	methods := ["Invoke"]
	bound := []
	vtbl := "Buffer"
	evt := "Buffer"
	ptr := 0
	
	__New(self := "")
	{
		this.self := self
		self := self != "" ? self : this
		
		this.SetCapacity("vtbl", (3+this.methods.Count()) * A_PtrSize)
		pvtbl := this.GetAddress("vtbl")
		
		NumPut(RegisterCallback(this.QueryInterface,,, &this), pvtbl+0*A_PtrSize, "UPtr")
		NumPut(RegisterCallback(this.AddRef,,, &this), pvtbl+1*A_PtrSize, "UPtr")
		NumPut(RegisterCallback(this.Release,,, &this), pvtbl+2*A_PtrSize, "UPtr")
		
		for i, method in this.methods
		{
			bind := {obj: &self, method: this[method]}
			this.bound.Push(bind)
			NumPut(RegisterCallback(this._Method,, this[method].MinParams, &bind)
			, pvtbl+(A_Index+2)*A_PtrSize, "UPtr")
		}
		
		this.SetCapacity("evt", A_PtrSize+4)
		this.ptr := this.GetAddress("evt")
		NumPut(pvtbl, this.ptr, 0, "UPtr")
		NumPut(1, this.ptr, A_PtrSize, "UInt")
	}
	
	__Delete()
	{
		; Free the callbacks
	}
	
	_Method(pp*)
	{
		p := []
		ctx := Object(A_EventInfo)
		loop, % ctx.method.MinParams-1
			p.Push(NumGet(pp+(A_Index-1)*A_PtrSize))
		ctx.method.Call(Object(ctx.obj), p*)
	}
	
	QueryInterface(iid, ppvObject)
	{
		; TODO
	}
	
	AddRef()
	{
		return ObjAddRef(A_EventInfo)
	}
	
	Release()
	{
		return ObjRelease(A_EventInfo)
	}
}
