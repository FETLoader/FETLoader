﻿#Include Lib\IDispatch.ahk

AsyncRoutine(fname, types:="")
{
	minParams := (IsObject(fname) ? fname : Func(fname)).MinParams
	return {"base": Func("AsyncRoutine_Binder").Bind(fname), "MinParams": minParams}
}

AsyncRoutine_Binder(fname, params*)
{
	bound := Func("AsyncRoutine_Proxy").Bind(fname, params)
	SetTimer, %bound%, -0
	return "called asynchronously"
}

AsyncRoutine_Proxy(fname, params)
{
	;Print("Proxy", fname, params)
	; Wrap IDispatch parameters
	for k, v in params
		if (ComObjType(v) == IDispatch.VT_DISPATCH)
			params[k] := new IDispatch(ComObjValue(v))
	
	%fname%(params*)
}
