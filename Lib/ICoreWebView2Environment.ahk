﻿#Include Lib\_COM_Meta.ahk

class ICoreWebView2Environment extends _COM_Meta
{
	CreateCoreWebView2Controller(parentWindow, handler)
	{
		if (r := DllCall(NumGet(NumGet(this.ptr)+3*A_PtrSize)
			, "Ptr", this.ptr     ; ICoreWebView2Environment * This
			, "Ptr", parentWindow ; HWND parentWindow
			, "Ptr", handler      ; ICoreWebView2CreateCoreWebView2ControllerCompletedHandler *handler
			, "Int"))             ; HRESULT
			throw Exception(r)
	}
	
	CreateWebResourceResponse(content, statusCode, reasonPhrase, headers)
	{
		; 4
		; HRESULT ( STDMETHODCALLTYPE *CreateWebResourceResponse )(
		;     ICoreWebView2Environment * This,
		;     /* [in] */ IStream *content,
		;     /* [in] */ int statusCode,
		;     /* [in] */ LPCWSTR reasonPhrase,
		;     /* [in] */ LPCWSTR headers,
		;     /* [retval][out] */ ICoreWebView2WebResourceResponse **response);
	}
	
	BrowserVersionString[]
	{
		get
		{
			if (r := DllCall(NumGet(NumGet(this.ptr)+5*A_PtrSize)
				, "Ptr", this.ptr ; ICoreWebView2Environment * This
				, "Ptr*", pVI     ; LPWSTR *versionInfo
				, "Int"))         ; HRESULT
				throw Exception(r)
			this._Get_Str(pVI, versionInfo)
			return versionInfo
		}
	}
	
	add_NewBrowserVersionAvailable(eventHandler, ByRef token)
	{
		; 6
		; HRESULT ( STDMETHODCALLTYPE *add_NewBrowserVersionAvailable )(
		;     ICoreWebView2Environment * This,
		;     /* [in] */ ICoreWebView2NewBrowserVersionAvailableEventHandler *eventHandler,
		;     /* [out] */ EventRegistrationToken *token);
	}
	
	remove_NewBrowserVersionAvailable(ByRef token)
	{
		; 7
		; HRESULT ( STDMETHODCALLTYPE *remove_NewBrowserVersionAvailable )(
		;     ICoreWebView2Environment * This,
		;     /* [in] */ EventRegistrationToken token);
	}
}
