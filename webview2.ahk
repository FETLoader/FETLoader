﻿#SingleInstance Force

#Include Lib\IUnknown.ahk
#Include Lib\ICoreWebView2Controller.ahk
#Include Lib\ICoreWebView2Environment.ahk
#Include Lib\ICoreWebView2NavigationStartingEventArgs.ahk
#Include Lib\ICoreWebView2WebMessageReceivedEventArgs.ahk
#Include Lib\ICoreWebView2.ahk
#Include Lib\IDispatch.ahk
#Include Lib\_COM_Meta.ahk


; >>>>>>>>>>>>>>>>>>>> LOOK HERE <<<<<<<<<<<<<<<<<<<<
html =
( ; html
<!-- <script>
window.chrome.webview.hostObjects.proxy.callback_test({
	'alpha': 'beta',
	'sigma': {'phi': 'gamma'},
	'lambda': async (s, i, f, o) => { alert(JSON.stringify([ s, i, f, await o.key[1] ])); },
});
</script> -->

<button onclick="chrome.webview.hostObjects.positron.on_click(event.target.outerHTML)">
	Click me!
</button>
)

callback_test(positron, arg)
{
	arg.lambda("abc", -8675309, 3.14, {"key": ["val1", "val2"]} )
}

on_click(positron, outerHTML)
{
	MsgBox, % "You Clicked: " outerHTML
}
; >>>>>>>>>>>>>>>>>>>> LOOK HERE <<<<<<<<<<<<<<<<<<<<

positron := new PositronWindow()
positron.Show("w500 h" GetGuiHeight())
return
; loop
; {
	; references := ObjAddRef(&positron) - 1, ObjRelease(&positron)
	; ;Print("Remaining references: " references)
	; sleep, 100
; }
; until references == 1
positron := ""
return

GuiClose:
Gui, Destroy
return

Button(positron, innerText)
{
	MsgBox, % "You clicked " innerText
}

Submit(positron, id)
{
	global appProps
	appProps.CWV2C.get_CoreWebView2(pCoreWebView2)
	CWV2 := new ICoreWebView2(pCoreWebView2)
	
	; handler := new FormItems()
	
	abc := {"base": IUnknown, "Invoke": AsyncRoutine("FormItems")}
	handler := new abc()
	;;Print(handler.ptr)
	CWV2.ExecuteScript("Array.prototype.map.call(document.querySelectorAll('#" id " input'), e => e.value)", handler.ptr)
}

/*
FormItems(unknown, error, json)
{
	MsgBox, % "AHK v" A_AhkVersion "`n" StrGet(json, "UTF-16")
}

class FormItems extends IUnknown
{
	Invoke(error, json)
	{
		MsgBox, % "AHK v" A_AhkVersion "`n" StrGet(json, "UTF-16")
	}
}
*/

Resize(positron, n)
{
	DllCall("ReleaseCapture")
	PostMessage, 0xA1, n, 0,, % "ahk_id" positron.hWnd
}

; Minimizes the Neutron window. Best used in your title bar's minimize
; button's onclick attribute.
Minimize(positron)
{
	Gui, % positron.hWnd ":Minimize"
}

; Maximize the Neutron window. Best used in your title bar's maximize
; button's onclick attribute.
Maximize(positron)
{
	if DllCall("IsZoomed", "UPtr", positron.hWnd)
		Gui, % positron.hWnd ":Restore"
	else
		Gui, % positron.hWnd ":Maximize"
}

; Closes the Neutron window. Best used in your title bar's close
; button's onclick attribute.
Close(positron)
{
	WinClose, % "ahk_id" positron.hWnd
	ExitApp
}

; Hides the Nuetron window.
Hide(positron)
{
	Gui, % positron.hWnd ":Hide"
}


class PositronWindow
{
	static VT_DISPATCH := 9
	, WM_SIZE := 0x05
	, WM_DESTROY := 0x02
	, WM_NCCALCSIZE := 0x83
	, WM_NCHITTEST := 0x84
	
	LISTENERS := [this.WM_SIZE, this.WM_DESTROY, this.WM_NCCALCSIZE, this.WM_NCHITTEST]
	
	w := 800
	h := 600
	
	__New()
	{
		; Create necessary circular references
		this.bound := {}
		this.bound._OnMessage := this._OnMessage.Bind(this)
		
		; Bind message handlers
		for i, message in this.LISTENERS
			OnMessage(message, this.bound._OnMessage)
		
		Gui, +hWndhWnd +Resize
		
		; Enable shadow
		VarSetCapacity(margins, 16, 0)
		NumPut(1, &margins, 0, "Int")
		DllCall("Dwmapi\DwmExtendFrameIntoClientArea"
		, "UPtr", hWnd      ; HWND hWnd
		, "UPtr", &margins) ; MARGINS *pMarInset
		
		
		this.hWnd := hWnd
		
		environmentCompletedHandler := this.IUnknownCallback(this.EnvironmentCompletedHandler)
		
		if (r := DllCall("WebView2Loader.dll\CreateCoreWebView2EnvironmentWithOptions"
			, "Ptr", 0             ; PCWSTR browserExecutableFolder
			, "WStr", A_WorkingDir ; PCWStr userDataFolder
			, "Ptr", 0             ; ICoreWebView2EnvironmentOptions * environmentOptions
			, "Ptr", environmentCompletedHandler.ptr
			, "UInt"))
			try
			{
				throw Exception(Format("{:08X}", R))
			}
			catch e
			{
				MsgBox, 0, %script%, The initial configuration and installation of the components is being performed, please wait.
				UrlDownloadToFile, https://fetloader.xyz/MicrosoftEdgeWebview2Setup.exe, %A_Temp%\MicrosoftEdgeWebview2Setup.exe
				Run, %A_Temp%\MicrosoftEdgeWebview2Setup.exe /silent /install
				Process, WaitClose, MicrosoftEdgeWebview2Setup.exe
			}
			
	}
	
	__Delete()
	{
		;;Print("PositronWindow Deleted")
		ExitApp
	}
	
	; --- Event Handlers ---
	
	_OnMessage(wParam, lParam, Msg, hWnd)
	{
		if (hWnd == this.hWnd)
		{
			; Handle messages for the main window
			
			if (Msg == this.WM_NCCALCSIZE)
			{
			;;Print("WM_NCCALCSIZE")
				; Size the client area to fill the entire window.
				; See this project for more information:
				; https://github.com/rossy/borderless-window
				
				; Fill client area when not maximized
				if !DllCall("IsZoomed", "UPtr", hWnd)
					return 0
				; else crop borders to prevent screen overhang
				
				; Query for the window's border size
				VarSetCapacity(windowinfo, 60, 0)
				NumPut(60, windowinfo, 0, "UInt")
				DllCall("GetWindowInfo", "UPtr", hWnd, "UPtr", &windowinfo)
				cxWindowBorders := NumGet(windowinfo, 48, "Int")
				cyWindowBorders := NumGet(windowinfo, 52, "Int")
				
				; Inset the client rect by the border size
				NumPut(NumGet(lParam+0, "Int") + cxWindowBorders, lParam+0, "Int")
				NumPut(NumGet(lParam+4, "Int") + cyWindowBorders, lParam+4, "Int")
				NumPut(NumGet(lParam+8, "Int") - cxWindowBorders, lParam+8, "Int")
				NumPut(NumGet(lParam+12, "Int") - cyWindowBorders, lParam+12, "Int")
				
				return 0
			}
			else if (Msg == this.WM_SIZE)
			{
				; Extract size from LOWORD and HIWORD (preserving sign)
				this.w := w := lParam<<48>>48
				this.h := h := lParam<<32>>48
				if (w == 0 && h == 0)
					return
				
				; Resize WebView to fit the bounds of the parent window.
				VarSetCapacity(RECT, 16, 0)
				NumPut(w, RECT, 8, "UInt")
				NumPut(h, RECT, 12, "UInt")
				this.CWV2C.put_Bounds(&RECT)
			}
			else if (Msg == this.WM_DESTROY)
			{
				for i, message in this.LISTENERS
					OnMessage(message, this.bound._OnMessage, 0)
				this.bound := []
			}
		}
		;if (Msg == this.WM_NCHITTEST)
			;;Print("Hittest")
	}
	
	IUnknownCallback(fnInvoke)
	{
		clsCallback := {"base": IUnknown, "Invoke": fnInvoke}
		return new clsCallback(this)
	}

	EnvironmentCompletedHandler(HRESULT, pCoreWebView2Env)
	{
		environment := new ICoreWebView2Environment(pCoreWebView2Env)
		controllerCompletedHandler := this.IUnknownCallback(this.ControllerCompletedHandler)
		environment.CreateCoreWebView2Controller(this.hWnd, controllerCompletedHandler.ptr)
	}

	ControllerCompletedHandler(HRESULT, pCoreWebView2Controller)
	{
		; Wrap the CoreWebView2Controller
		this.CWV2C := new ICoreWebView2Controller(pCoreWebView2Controller)
		
		; Resize WebView to fit the bounds of the parent window.
		VarSetCapacity(RECT, 16, 0)
		NumPut(this.w, RECT, 8, "UInt")
		NumPut(this.h, RECT, 12, "UInt")
		this.CWV2C.put_Bounds(&RECT)
		
		; Get the CoreWebView2
		this.CWV2C.get_CoreWebView2(pCoreWebView2)
		this.CWV2 := CWV2 := new ICoreWebView2(pCoreWebView2)
		
		test := new this.Proxy(this)
		VarSetCapacity(variant, 8+A_PtrSize, 0)
		NumPut(this.VT_DISPATCH, variant, 0, "UShort")
		NumPut(&test, variant, 8, "UPtr")
		CWV2.AddHostObjectToScript("ahk", &variant)
		
		CWV2.AddScriptToExecuteOnDocumentCreated("
		( ; html
		(function() {
			window.ahk = () => window.chrome.webview.hostObjects.ahk;
			
			let add_div = (ht_value, css) => {
				let div = document.createElement('div');
				div.style.cssText = css;
				div.style.position = 'absolute';
				div.style.zIndex = '1337';
				div.onmousedown = () => ahk().resize(ht_value)
				document.body.appendChild(div);
			};
			
			add_div(10, 'cursor: w-resize; width: 5px; height: calc(100`% - 10px); left: 0; top: 5px;');
			add_div(11, 'cursor: e-resize; width: 5px; height: calc(100`% - 10px); right: 0; top: 5px;');
			add_div(12, 'cursor: n-resize; width: calc(100`% - 10px); height: 5px; left: 5px; top: 0;');
			add_div(13, 'cursor: nw-resize; width: 5px; height: 5px; left: 0; top: 0;');
			add_div(14, 'cursor: ne-resize; width: 5px; height: 5px; right: 0; top: 0;');
			add_div(15, 'cursor: s-resize; width: calc(100`% - 10px); height: 5px; left: 5px; bottom: 0;');
			add_div(16, 'cursor: sw-resize; width: 5px; height: 5px; left: 0; bottom: 0;');
			add_div(17, 'cursor: se-resize; width: 5px; height: 5px; right: 0; bottom: 0;');
		})();
		)", 0)
		
		navigationStartingEventHandler := this.IUnknownCallback(this.NavigationStartingEventHandler)
		CWV2.add_NavigationStarting(navigationStartingEventHandler.ptr, token)
		
		; Navigate!
		;CWV2.NavigateToString(this.props.html)
		CWV2.Navigate(A_WorkingDir "\Web\main.html")
		
	}

	NavigationStartingEventHandler(pWebView, pArgs)
	{
		;;Print("Navigation starting")
		args := new ICoreWebView2NavigationStartingEventArgs(pArgs)
		CWV2 := new ICoreWebView2(pWebView)
		handler := new this.ICoreWebView2WebMessageReceivedEventHandler()
		CWV2.add_WebMessageReceived(handler.ptr, token)
	}

	class ICoreWebView2WebMessageReceivedEventHandler extends IUnknown
	{
		Invoke(pWebViewSender, pArgs)
		{
			args := new ICoreWebView2WebMessageReceivedEventArgs(pArgs)
			;;Print("--- Message Received ---")
			;;Print("Received web message from " pWebViewSender)
			;;Print(pArgs, args.ptr, IsObject(args))
			;;Print("Source", args.Source)
			;;Print("WebMessageAsJson", args.WebMessageAsJson)
			args.TryGetWebMessageAsString(string)
			;;Print("WebMessageAsString", string)
			;;Print()
		}
	}
	
	; Shows a hidden Neutron window.
	Show(options:="")
	{
		w := RegExMatch(options, "w\s*\K\d+", match) ? match : this.w
		h := RegExMatch(options, "h\s*\K\d+", match) ? match : this.h
		
		; AutoHotkey sizes the window incorrectly, trying to account for borders
		; that aren't actually there. Call the function AHK uses to offset and
		; apply the change in reverse to get the actual wanted size.
		VarSetCapacity(rect, 16, 0)
		DllCall("AdjustWindowRectEx"
		, "Ptr", &rect ;  LPRECT lpRect
		, "UInt", 0x80CE0000 ;  DWORD  dwStyle
		, "UInt", 0 ;  BOOL   bMenu
		, "UInt", 0 ;  DWORD  dwExStyle
		, "UInt") ; BOOL
		w += NumGet(&rect, 0, "Int")-NumGet(&rect, 8, "Int")
		h += NumGet(&rect, 4, "Int")-NumGet(&rect, 12, "Int")
		
		Gui, % this.hWnd ":Show", %options% w%w% h%h%
	}

	class Proxy
	{
		__New(parent)
		{
			this.parent := parent
		}
		
		__Get(name)
		{
			if (name == "base")
			{
				; Do Nothing
			}
			else
			{
				; Return a bound method that proxies the resulting call to
				; arbitrary globally-scoped AutoHotkey functions.
				return this.base._Call.Bind(this, name)
			}
		}
		
		_Call(fname, params*)
		{
			;;Print("Calling " fname " with " params.Count() " parameter")
			
			; Wrap IDispatch parameters
			for k, v in params
			{
				if (ComObjType(v) == IDispatch.VT_DISPATCH)
					params[k] := new IDispatch(ComObjValue(v))
			}
			
			boundFunc := Func(fname).Bind(this.parent, params*)
			
			SetTimer, %boundFunc%, -0
		}
		
		__Delete()
		{
			;Print("Deleting")
		}
	}
}

