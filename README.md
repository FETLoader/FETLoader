![Logo](https://i.imgur.com/eOSghI6.png)

### A simple CS:GO cheats loader written in AHK.

## Screenshots
![Dark](https://fetloader.xyz/dark.png)


## Features
- Web UI
- Custom repositories w/ cheats
- Big cheats library (no)
- Opensource
- A more shitcode

## How to compile
- Install AHK ANSI 32-bit
- Clone the repository with the command `git clone https://gitlab.com/FETLoader/FETLoader | cd FETLoader | git submodule update --init --recursive`
- Run `COMPILE.exe`

## Contributing

Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

## License

Distributed under the MIT License. See `LICENSE` for more information.


## Credits
- Loader: [clownless](https://clownless.xyz) & [rf0x3d](https://rf0x3d.su) & [iluvureyes](https://t.me/iluvureyes)
- Inject_Dll: [ a1987zz](https://github.com/a1987zz)
- Positron: [G33kDude](https://github.com/G33kDude)
- ExternalModuleBypasser: [0x000cb](https://github.com/0x000cb)
- VAC-Bypass-Loader: [danielkrupinski](https://github.com/danielkrupinski/VAC-Bypass-Loader)
- Ahk2Exe: [AutoHotkey](https://github.com/AutoHotkey/Ahk2Exe)
- Translation: [Gl1c1n](https://github.com/Gl1c1n)
- Web-UI: fuele
- Custom repo: [zrn1x](https://github.com/oliyase)
- Big thanks to MORGENSHTERN and his tracks

